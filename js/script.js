let visits = [];

class Visit {
    constructor(doctor, color, fio, date, purpose, comment) {
        this.fio = fio;
        this.doctor = doctor;
        this.date = date;
        this.purpose = purpose;
        this.comment = comment;
        this.left = Visit.MARGIN;
        this.top = Visit.MARGIN;
        this.color = color;
    }

    displayCard() {
        const card = document.createElement('div');
        card.className = 'new-event';
        card.style.backgroundColor = this.color;
        card.style.width = Visit.WIDTH.toString() + 'px';
        card.style.height = Visit.HEIGHT.toString() + 'px';
        card.style.left = this.left.toString() + 'px';
        card.style.top = this.top.toString() + 'px';

        const message = document.getElementById('empty');
        message.style = 'display: none';

        const cardDelete = document.createElement('button');
        cardDelete.className = 'new-event_delete';
        cardDelete.innerHTML = '<i class="far fa-times-circle"></i>';

        card.append(cardDelete);

        const cardFIO = document.createElement('p');
        cardFIO.className = 'new-event_fio';
        cardFIO.textContent = this.fio;
        card.append(cardFIO);

        const cardDoctor = document.createElement('p');
        cardDoctor.className = 'new-event_doctor';
        cardDoctor.textContent = this.doctor;
        card.append(cardDoctor);

        const cardOptions = document.createElement('div');
        cardOptions.class = 'new-event_options';
        card.append(cardOptions);

        const cardMore = document.createElement('button');
        cardMore.className = 'new-event_more';
        cardMore.textContent = 'показать больше';

        card.append(cardMore);

        card.onmousedown = (e) => {
            let coords = getCoords(card);
            let shiftX = e.pageX - coords.left;
            let shiftY = e.pageY - coords.top;
            let isDrag = false;

            card.style.position = 'absolute';
            document.body.appendChild(card);
            moveAt(e);

            card.style.zIndex = 1000;

            function moveAt(e) {
                card.style.left = e.pageX - shiftX + 'px';
                card.style.top = e.pageY - shiftY + 'px';
            }

            document.onmousemove = (e) => {
                isDrag = true;
                moveAt(e);
            };

            card.onmouseup = function() {
                document.onmousemove = null;
                card.onmouseup = null;
            };

            cardDelete.onmouseup = function () {
                if (!isDrag) {
                    visits.splice(visits.indexOf(this), 1);
                    localStorage.setItem('visits', JSON.stringify(visits));
                    card.remove();
                    if (!visits.length) empty.style = 'display: block';
                }
            };

            cardMore.onmouseup = () => {
                if (!isDrag) {
                    if (cardOptions.firstChild) {
                        while (cardOptions.firstChild) cardOptions.removeChild(cardOptions.firstChild);
                        cardMore.textContent = 'показать больше';
                    } else {
                        let fields = [];

                        if (this.doctor === 'Кардиолог') {
                            fields = Cardiologist.FIELDS;
                        } else if (this.doctor === 'Стоматолог') {
                            fields = Dentist.FIELDS;
                        } else if (this.doctor === 'Терапевт') {
                            fields = Therapist.FIELDS;
                        }
                        fields.forEach(value => {
                            const cardOption = document.createElement('p');
                            cardOption.class = 'new-event_option';
                            cardOption.innerHTML = value.caption + ': <span>' + this[value.name] +'</span>';
                            cardOptions.append(cardOption);
                        });

                        const cardComment = document.createElement('p');
                        cardComment.className = 'new-event_comment';
                        cardComment.innerHTML = 'Комментарий: <span>' + this.comment + '</span>';
                        cardOptions.append(cardComment);

                        cardMore.textContent = 'показать меньше';
                    }
                }
            }
        };

        card.ondragstart = function() {
            return false;
        };

        function getCoords(elem) {
            const box = elem.getBoundingClientRect();
            return {
                top: box.top + pageYOffset,
                left: box.left + pageXOffset
            };
        }

        board.append(card);
    }

    static displayModalWindow() {
        if(document.getElementById('modal-window') !== null) return;

        const modalBackground = document.createElement('div');
        modalBackground.className = 'modal-background';
        modalBackground.style.zIndex = 1001;
        modalBackground.onclick = event => {
            if (event.target === modalBackground) modalBackground.remove();
        };
        document.body.append(modalBackground);

        const modal = document.createElement('div');
        modal.className = 'modal-window';
        modalBackground.append(modal);

        const modalHeader = document.createElement('div');
        modalHeader.className = 'header_modal-window';
        modal.append(modalHeader);

        const modalHeaderText = document.createElement('h3');
        modalHeaderText.className = 'modal-window_text';
        modalHeaderText.textContent = 'Запись к врачу';
        modalHeader.append(modalHeaderText);

        const modalHeaderIcon = document.createElement('i');
        modalHeaderIcon.classList.add('far', 'fa-times-circle', 'fa-2x');
        modalHeaderIcon.onclick = () => modalBackground.remove();
        modalHeader.append(modalHeaderIcon);

        const modalOptions = document.createElement('div');
        modalOptions.className = 'options';
        modal.append(modalOptions);

        const modalFIO = document.createElement('input');
        modalFIO.type = 'text';
        modalFIO.className = 'fio-input';
        modalFIO.placeholder = 'ФИО';
        modalFIO.required = true;
        modalOptions.append(modalFIO);

        const modalDoctorSelect = document.createElement('select');
        modalDoctorSelect.className = 'doctor-select';
        modalDoctorSelect.onchange = () => {
            switch (modalDoctorSelect.selectedOptions[0].value) {
                case 'Кардиолог':
                    Visit.updateModalFields(Cardiologist.FIELDS);
                    break;
                case 'Стоматолог':
                    Visit.updateModalFields(Dentist.FIELDS);
                    break;
                case 'Терапевт':
                    Visit.updateModalFields(Therapist.FIELDS);
                    break;
                default:
                    Visit.updateModalFields([]);
            }
        };
        modalOptions.append(modalDoctorSelect);

        const modalDoctorDefaultOption = document.createElement('option');
        modalDoctorDefaultOption.textContent = 'Выберите доктора';
        modalDoctorSelect.append(modalDoctorDefaultOption);

        ['Кардиолог','Стоматолог','Терапевт'].forEach(value => {
            const modalDoctorOption = document.createElement('option');
            modalDoctorOption.value = value;
            modalDoctorOption.textContent = value;
            modalDoctorSelect.append(modalDoctorOption);
        });

        const modalAdvOptions = document.createElement('div');
        modalAdvOptions.id = 'advOptions';
        modalOptions.append(modalAdvOptions);

        const modalComment = document.createElement('textarea');
        modalComment.maxLength = 400;
        modalComment.placeholder = 'Комментарии';
        modalOptions.append(modalComment);

        const modalButton = document.createElement('button');
        modalButton.textContent = 'Записаться';
        modalButton.id = 'button_add';
        modalButton.onclick = () => {
            let newVisit = null;

            switch (modalDoctorSelect.selectedOptions[0].value) {
                case 'Кардиолог':
                    newVisit = new Cardiologist(
                        modalFIO.value,
                        date.value,
                        purpose.value,
                        pressure.value,
                        massIndex.value,
                        diseases.value,
                        age.value,
                        modalComment.value);
                    break;

                case 'Стоматолог':
                    newVisit = new Dentist(
                        modalFIO.value,
                        date.value,
                        purpose.value,
                        lastVisit.value,
                        modalComment.value);
                    break;

                case 'Терапевт':
                    newVisit = new Therapist(
                        modalFIO.value,
                        date.value,
                        purpose.value,
                        age.value,
                        modalComment.value);
                    break;
            }

            if (newVisit) {
                let leftOffset = Visit.MARGIN;
                let topOffset = Visit.MARGIN;
                while (visits.some(value => value.left === leftOffset && value.top === topOffset)) {
                    leftOffset += Visit.WIDTH + Visit.MARGIN;
                    if (leftOffset >= 1200) {
                        topOffset += Visit.HEIGHT + Visit.MARGIN;
                        leftOffset = Visit.MARGIN;
                    }
                }
                newVisit.left = leftOffset;
                newVisit.top = topOffset;

                visits.push(newVisit);
                localStorage.setItem('visits', JSON.stringify(visits));
                modalBackground.remove();
                empty.style = 'display: none';
                newVisit.displayCard();
            }
        };
        modalOptions.append(modalButton);
    }

    static updateModalFields(fields) {
        const modalAdvOptions = document.getElementById('advOptions');
        while (modalAdvOptions.firstChild) modalAdvOptions.removeChild(modalAdvOptions.firstChild);

        fields.forEach(value => {
            const modalInput = document.createElement('input');
            modalInput.type = 'text';
            modalInput.id = value.name;
            modalInput.placeholder = value.caption;
            modalInput.required = true;
            modalAdvOptions.append(modalInput);
        });
    }

    static WIDTH = 290;
    static HEIGHT = 280;
    static MARGIN = 8;
}

class Cardiologist extends Visit {
    constructor(fio, date, purpose, pressure, massIndex, diseases, age, comment = '') {
        super('Кардиолог', '#3366996b', fio, date, purpose, comment);
        this.pressure = pressure;
        this.massIndex = massIndex;
        this.diseases = diseases;
        this.age = age;
    }

    static FIELDS = [
        {name: 'date', caption: 'Дата визита'},
        {name: 'purpose', caption: 'Цель визита'},
        {name: 'pressure', caption: 'Обычное давление'},
        {name: 'massIndex', caption: 'Индекс массы тела'},
        {name: 'diseases', caption: 'Перенесенные заболевания'},
        {name: 'age', caption: 'Возраст'}
    ]
}
class Dentist extends Visit {
    constructor(fio, date, purpose, lastVisit, comment = '') {
        super('Стоматолог', '#66993387', fio, date, purpose, comment);
        this.lastVisit = lastVisit;
    }

    static FIELDS = [
        {name: 'date', caption: 'Дата визита'},
        {name: 'purpose', caption: 'Цель визита'},
        {name: 'lastVisit', caption: 'Дата последнего посещения'}
    ]
}
class Therapist extends Visit {
    constructor(fio, date, purpose, age, comment = '') {
        super('Терапевт', '#ff66008f', fio, date, purpose, comment);
        this.age = age;
    }

    static FIELDS = [
        {name: 'date', caption: 'Дата визита'},
        {name: 'purpose', caption: 'Цель визита'},
        {name: 'age', caption: 'Возраст'}
    ]
}

const renderContent = function() {
    const storageVisits = JSON.parse(localStorage.getItem('visits'));
    if(storageVisits) {
        let leftOffset = Visit.MARGIN;
        let topOffset = Visit.MARGIN;

        if (!storageVisits.length) empty.style = 'display: block';

        storageVisits.forEach(value => {
            let newVisit = null;

            switch (value.doctor) {
                case 'Кардиолог':
                    newVisit = new Cardiologist(
                        value.fio,
                        value.date,
                        value.purpose,
                        value.pressure,
                        value.massIndex,
                        value.diseases,
                        value.age,
                        value.comment);
                    break;

                case 'Стоматолог':
                    newVisit = new Dentist(
                        value.fio,
                        value.date,
                        value.purpose,
                        value.lastVisit,
                        value.comment);
                    break;

                case 'Терапевт':
                    newVisit = new Therapist(
                        value.fio,
                        value.date,
                        value.purpose,
                        value.age,
                        value.comment);
                    break;
            }

            if (newVisit) {
                newVisit.left = leftOffset;
                newVisit.top = topOffset;

                leftOffset += Visit.WIDTH + Visit.MARGIN;
                if (leftOffset >= 1200) {
                    topOffset += Visit.HEIGHT + Visit.MARGIN;
                    leftOffset = Visit.MARGIN;
                }

                visits.push(newVisit);
                newVisit.displayCard();
            }
        });
    } else {
        empty.style = 'display: block'
    }

    add_new_event.onclick = () => Visit.displayModalWindow();
};

document.addEventListener('DOMContentLoaded', renderContent);
